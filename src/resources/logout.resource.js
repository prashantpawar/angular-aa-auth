(function () {
    'use strict';

    angular.module('aaAuth')
            .factory('logoutResource', logoutResource);

    logoutResource.$inject = ['$resource'];
    
    function logoutResource($resource) {

        var url = Settings.apiDealDocumentsUrl;

        var params = {};

        var actions = {
            get: {
                method: 'POST',
//                transformResponse: function transformResponse(data, headersGetter) {
//                    var json = angular.fromJson(data);
//                    return new dealDocumentModel(json);
//                },
//                interceptor: {
//                    response: function (response) {
//                        var deferred = $q.defer();
//
//                        deferred.resolve(response.data);
//
//                        return deferred.promise;
//                    }
//                }
            }
        };

        return $resource(url, params, actions);
    }

}());
