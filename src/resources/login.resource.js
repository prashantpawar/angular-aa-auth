(function () {
    'use strict';

    angular.module('aaAuth')
            .factory('loginResource', loginResource);

    loginResource.$inject = ['$resource'];
    function loginResource($resource) {

        var url = '/api/accounts/auth';

        var params = {
            pk: '@pk'
        };

        var actions = {
            post: {
                method: 'POST',
            }
        };

        return $resource(url, params, actions);
    }

}());
