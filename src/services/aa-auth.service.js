(function () {
    'use strict';

    angular.module('aaAuth')
            .factory('aaAuthService', aaAuthService);

    aaAuthService.$inject = ['loginResource', 'logoutResource'];
    
    function aaAuthService(loginResource, logoutResource) {
        var _user;

        var service = {
            login: login,
            logout: logout,
            isLoggedIn: isLoggedIn,
            currentUser: currentUser
        };

        return service;

        function login(username, password) {
            var params = {
                email: username,
                password: password
            };
            
            var promise = loginResource.post(params).$promise;
            
            promise.then(successCallback, errorCallback);
            
            return promise;
            
            function successCallback(data) {
                _user = data;
            }
            
            function errorCallback() {
                clear();
            }
        }

        function logout() {
            var params = {};
            
            var promise = logoutResource.post(params).$promise;
            
            promise.then(successCallback, errorCallback);
            
            return promise;
            
            function successCallback() {
                clear();
            }
            
            function errorCallback() {
                clear();
            }
        }

        function isLoggedIn() {
            return false;
        }

        function currentUser() {
            return _user;
        }
        
        function clear() {
            _user = null;
        }
    }

}());
