(function () {
    'use strict';

    angular.module('aaAuth')
            .directive('aaAuthLoginForm', aaAuthLoginFormDirective);

    function aaAuthLoginFormDirective() {
        return {
            link: link,
            restrict: 'A'
        };

        function link(scope, elem, attrs) {
        }
    }
}());
